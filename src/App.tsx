import { useState } from 'react'
import './App.css'
import { defaultChartLayout } from './components/TradeViewChart/utils/constants'
import TradeViewChart from './components/TradeViewChart'
import Header from './components/Header'
import TradeBrokerTabs from './components/TradeBrokerTabs'
import TradeAbsTabs from './components/TradeAbsTabs'
import TradeExchangeBar from './components/TradeExchangeBar'

function App() {
  return (
    <div className="App">
      <Header />

      <div className="trade-wrap">
        <div className="trade-content">
          <TradeExchangeBar />
          <div className="trade-time-info">time</div>
          <TradeViewChart
            candleStickConfig={{
              upColor: '#34A344',
              downColor: '#cf304a',
              wickDownColor: '#FF3D3A',
              wickUpColor: '#34A344',
              borderDownColor: '#cf304a',
              borderUpColor: '#34A344',
            }}
            containerStyle={{
              minHeight: '300px',
              minWidth: '400px',
              marginBottom: '0',
              height: '100%',
              width: '100%',
              display: 'flex',
              // flex: 1
            }}
            chartLayout={{
              ...defaultChartLayout,
              layout: {
                backgroundColor: '#FAFAFA',
              },
            }}
            pair="BTCBUSD"
          />
        </div>
        <TradeBrokerTabs />
        <TradeAbsTabs />
        {/*<div className="trade-side">
          <TradeTabs />
          {/*<TradeTabs />
        </div>*/}
      </div>
    </div>
  )
}

export default App
