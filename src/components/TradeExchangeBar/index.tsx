import './TradeExchangeBar.css'
import React, { FC } from 'react'
import { Menu, Dropdown } from 'antd'
import { DownOutlined } from '@ant-design/icons'

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="https://www.antgroup.com">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="https://www.aliyun.com">2nd menu item</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
)

const TradeExchangeBar: FC = () => {
  return (
    <div className="TradeExchangeBar">
      <Dropdown overlay={menu} trigger={['click']}>
        <a
          className="ant-dropdown-link trade-info separator-right"
          onClick={(e) => e.preventDefault()}
        >
          <div className="dropdown-left ">
            <span className="symbol-title">BTC/USDT</span>
            <span className="symbol-price">$46,021.79</span>
          </div>{' '}
          <DownOutlined />
        </a>
      </Dropdown>
      <div className="trade-info separator-right">
        <div className="trade-info-col">
          <span className="info-title">27h Change</span>
          <span className="info-price">-1,291.85 - 2.73%</span>
        </div>
      </div>
      <div className="trade-info separator-right">
        <div className="trade-info-col">
          <span className="info-title">24h High</span>
          <span className="info-price">48,300.01</span>
        </div>
      </div>
      <div className="trade-info separator-right">
        <div className="trade-info-col">
          <span className="info-title">24h Low</span>
          <span className="info-price">45,963.00</span>
        </div>
      </div>
      <div className="trade-info separator-right">
        <div className="trade-info-col">
          <span className="info-title">24h Volume (BTC)</span>
          <span className="info-price">30,603.51</span>
        </div>
      </div>
      <div className="trade-info separator-right">
        <div className="trade-info-col">
          <span className="info-title">24h Volume (USDT)</span>
          <span className="info-price">1,436,390,801.40</span>
        </div>
      </div>
    </div>
  )
}

export default TradeExchangeBar
