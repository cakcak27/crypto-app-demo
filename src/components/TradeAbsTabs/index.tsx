// ABS (Assets, Buy , Sell)
import React, { FC } from 'react'
import { Row, Col } from 'antd';
import './TradeAbsTabs.css'

const TradeAbsTabs:FC = () =>{
	return <div className="TradeAbsTabs">
		<div className="assets">
			<Row>
		      <Col span={6}>Assets</Col>
		      <Col span={18}>
		      	<Row>
		      		<Col className="price" span={16}>
		      			1.00002475823
		      		</Col>
		      		<Col span={8}>
		      			BTC
		      		</Col>
		      	</Row>
		      	<Row>
		      		<Col className="price" span={16}>
		      			672,947.95
		      		</Col>
		      		<Col span={8}>
		      			USDT
		      		</Col>
		      	</Row>
		      </Col>
		    </Row>
		</div>
	</div>
}

export default TradeAbsTabs;