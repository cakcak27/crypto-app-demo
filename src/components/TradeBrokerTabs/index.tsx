// information from each broker market

import React, { FC, useState } from 'react'
import { Tabs } from 'antd'
import './TradeBrokerTabs.css'
const { TabPane } = Tabs

interface Coin {
  logo: string
  name: string
  status: string
  price: float
}

enum SortBy {
  price = 'price',
  name = 'name',
}

enum SortType {
  ASC = 'ASC',
  DESC = 'DESC',
}

const ListCoin: FC<{ list: Coin[]; sortBy: SortBy; sortType: SortType }> = ({
  list,
  sortBy,
  sortType,
}) => {
  const [listCoin, useListCoint] = useState(list)

  switch (sortType) {
    case SortType.ASC:
      listCoin.sort((a, b) => {
        if (a[sortBy] == b[sortBy]) {
          return 0
        } else if (a[sortBy] < b[sortBy]) {
          return -1
        } else if (a[sortBy] > b[sortBy]) {
          return 1
        }
      })
      break
    case SortType.DESC:
      listCoin.sort((a, b) => {
        if (a[sortBy] == b[sortBy]) {
          return 0
        } else if (a[sortBy] < b[sortBy]) {
          return 1
        } else if (a[sortBy] > b[sortBy]) {
          return -1
        }
      })
      break
  }

  return (
    <div>
      {listCoin.map((coin, key) => (
        <div key={key}>
          {coin.logo}-{coin.name} - {coin.price}
        </div>
      ))}
    </div>
  )
}

const TabContent: FC = () => {
  let listCoin: Coin[] = [
    {
      logo: 'Unvailable',
      name: 'Indodax',
      status: 'buy',
      price: 46900.5,
    },
    {
      logo: 'Unvailable',
      name: 'Binance',
      status: 'sell',
      price: 46800.5,
    },
    {
      logo: 'Unvailable',
      name: 'Coinbase',
      status: 'buy',
      price: 46800.5,
    },
  ]

  return (
    <div>
      <div>
        <button>{SortType.DESC}</button>
      </div>
      <ListCoin
        list={listCoin}
        sortBy={SortBy.price}
        sortType={SortType.DESC}
      />
    </div>
  )
}

const initialPanes = [
  { title: 'BTC/ USDT', content: <TabContent />, key: '1' },
  { title: 'ETH/ USDT', content: 'Content of Tab 2', key: '2' },
]

const TradeBrokerTabs: FC = () => {
  const [active, useActive] = useState(initialPanes[0].key)
  const [panes, usePanes] = useState(initialPanes)

  let newTabIndex = panes.length

  const onChange = (activeKey) => {
    useActive(activeKey)
  }

  const add = () => {
    const activeKey = `newTab${newTabIndex++}`
    const newPanes = [...panes]
    newPanes.push({
      title: `New Tab ${newTabIndex}`,
      content: 'Content of new Tab',
      key: activeKey,
    })

    usePanes(newPanes)
    useActive(activeKey)
  }

  const remove = (targetKey) => {
    // const { panes, activeKey } = this.state
    let newActiveKey = active
    let lastIndex
    panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1
      }
    })
    const newPanes = panes.filter((pane) => pane.key !== targetKey)
    if (newPanes.length && newActiveKey === targetKey) {
      if (lastIndex >= 0) {
        newActiveKey = newPanes[lastIndex].key
      } else {
        newActiveKey = newPanes[0].key
      }
    }
    usePanes(newPanes)
    useActive(active)
  }

  const onEdit = (targetKey, action) => {
    switch (action) {
      case 'remove':
        remove(targetKey)
        break
      case 'add':
        add()
        break
    }
  }

  return (
    <Tabs
      type="editable-card"
      onChange={onChange}
      activeKey={active}
      onEdit={onEdit}
    >
      {panes.map((pane) => (
        <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
          {pane.content}
        </TabPane>
      ))}
    </Tabs>
  )
}

export default TradeBrokerTabs
