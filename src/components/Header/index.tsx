import React from 'react'
import './Header.css'

const Header: React.FC = () => {
  return (
    <div className="header">
      <div className="logo">
        <p>UCLICK</p>
      </div>

      <div className="nav-header">
        <a className="nav-item">Buy Crypto</a>
        <a className="nav-item">Market</a>
        <a className="nav-item">Trade</a>
        <a className="nav-item">NFT</a>

        <div className="nav-right">
          <div className="switch-mode"></div>

          <div className="separator-none">
            <a className="nav-item">Wallet</a>
          </div>
          <div className="separator-left">
            <a className="nav-item">English</a>
          </div>
          <div className="separator-left">
            <a className="nav-item">Usd</a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header
